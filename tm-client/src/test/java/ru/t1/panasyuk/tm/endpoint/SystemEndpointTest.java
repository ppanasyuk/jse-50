package ru.t1.panasyuk.tm.endpoint;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.endpoint.ISystemEndpoint;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.request.system.ServerAboutRequest;
import ru.t1.panasyuk.tm.dto.request.system.ServerVersionRequest;
import ru.t1.panasyuk.tm.dto.response.system.ServerAboutResponse;
import ru.t1.panasyuk.tm.dto.response.system.ServerVersionResponse;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.service.PropertyService;

@Category(SoapCategory.class)
@DisplayName("Тестирование эндпоинта System")
public class SystemEndpointTest {

    @NotNull
    private static ISystemEndpoint systemEndpoint;

    @Before
    public void initEndpoint() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        systemEndpoint = ISystemEndpoint.newInstance(propertyService);
    }

    @Test
    @DisplayName("Получение информации о приложении")
    public void aboutTest() {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getName());
        Assert.assertNotNull(response.getApplicationName());
        Assert.assertNotNull(response.getEmail());
        Assert.assertNotNull(response.getGitCommitMessage());
        Assert.assertNotNull(response.getGitBranch());
        Assert.assertNotNull(response.getGitCommitId());
        Assert.assertNotNull(response.getGitCommitterEmail());
        Assert.assertNotNull(response.getGitCommitterName());
        Assert.assertNotNull(response.getGitCommitTime());
    }

    @Test
    @DisplayName("Получение информации о версии приложения")
    public void versionTest() {
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getVersion());
    }

}