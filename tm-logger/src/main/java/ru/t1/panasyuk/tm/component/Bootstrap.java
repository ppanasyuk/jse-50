package ru.t1.panasyuk.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.IReceiverService;
import ru.t1.panasyuk.tm.listener.EntityListener;
import ru.t1.panasyuk.tm.service.ReceiverService;

public final class Bootstrap {

    public void init() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new EntityListener());
    }

}