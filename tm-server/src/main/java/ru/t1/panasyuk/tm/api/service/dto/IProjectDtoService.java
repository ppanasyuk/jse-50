package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDTO> {

    @NotNull
    ProjectDTO changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    ProjectDTO create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@NotNull String userId, @Nullable String name);

    void clear();

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    List<ProjectDTO> findAll(@NotNull String userId, @Nullable Sort sort);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void update(@NotNull ProjectDTO project);

}