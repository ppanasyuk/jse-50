package ru.t1.panasyuk.tm.repository.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория задач DTO")
public class TaskDtoRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String testUser1Id;

    @NotNull
    private String testUser2Id;

    @NotNull
    private String project1Id;

    @NotNull
    private String project2Id;

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private ITaskDtoRepository taskRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws SQLException, IOException, LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final UserDTO user1 = new UserDTO();
        testUser1Id = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        testUser2Id = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDTO project1 = new ProjectDTO();
        project1Id = project1.getId();
        @NotNull final ProjectDTO project2 = new ProjectDTO();
        project2Id = project2.getId();
        projectRepository.add(testUser1Id, project1);
        projectRepository.add(testUser2Id, project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i < 5) {
                task.setUserId(testUser1Id);
                task.setProjectId(project1Id);
            } else {
                task.setUserId(testUser2Id);
                task.setProjectId(project2Id);
            }
            taskList.add(task);
            taskRepository.add(task);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    @DisplayName("Добавление задачи")
    public void addTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "Task Name";
        @NotNull final String taskDescription = "Task Description";
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(testUser1Id, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final TaskDTO createdTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(testUser1Id, createdTask.getUserId());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    @DisplayName("Добавление Null задачи")
    public void addNullTest() {
        @Nullable final TaskDTO createdTaskDTO = taskRepository.add(testUser1Id, null);
        Assert.assertNull(createdTaskDTO);
    }

    @Test
    @DisplayName("Добавление списка задач")
    public void addAllTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        @NotNull final String firstTaskDTOName = "First TaskDTO Name";
        @NotNull final String firstTaskDTODescription = "TaskDTO Description";
        @NotNull final TaskDTO firstTaskDTO = new TaskDTO();
        firstTaskDTO.setName(firstTaskDTOName);
        firstTaskDTO.setDescription(firstTaskDTODescription);
        tasks.add(firstTaskDTO);
        @NotNull final String secondTaskDTOName = "Second TaskDTO Name";
        @NotNull final String secondTaskDTODescription = "TaskDTO Description";
        @NotNull final TaskDTO secondTaskDTO = new TaskDTO();
        secondTaskDTO.setName(secondTaskDTOName);
        secondTaskDTO.setDescription(secondTaskDTODescription);
        tasks.add(secondTaskDTO);
        @NotNull final Collection<TaskDTO> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все задачи для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(testUser1Id);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(testUser1Id));
    }

    @Test
    @DisplayName("Поиск всех задач")
    public void findAllTest() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @DisplayName("Поиск всех задач по Id проекта")
    public void findAllByProjectIdTest() {
        @NotNull List<TaskDTO> taskListForProject = taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .filter(m -> project1Id.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(testUser1Id, project1Id);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    @DisplayName("Поиск всех задач по компаратору")
    public void findAllWithComparatorTest() {
        @NotNull Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = taskRepository.findAll().size();
        @NotNull List<TaskDTO> tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя")
    public void findAllForUserTest() {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(testUser1Id);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Поиск всех задач для пользователя по компаратору")
    public void findAllWithComparatorForUser() {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .filter(m -> project1Id.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<TaskDTO> tasks = taskRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskListForUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Поиск задачи по Id")
    public void findOneByIdTest() {
        @Nullable TaskDTO task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTaskDTO);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById("qwerty");
        Assert.assertNull(foundTaskDTO);
        @Nullable final TaskDTO foundTaskDTONull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskDTONull);
    }

    @Test
    @DisplayName("Поиск задачи по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable TaskDTO task;
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskListForUser.get(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(testUser1Id, taskId);
            Assert.assertNotNull(foundTaskDTO);
        }
    }

    @Test
    @DisplayName("Поиск задачи по Id равному Null для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final TaskDTO foundTaskDTO = taskRepository.findOneById(testUser1Id, "qwerty");
        Assert.assertNull(foundTaskDTO);
        @Nullable final TaskDTO foundTaskDTONull = taskRepository.findOneById(testUser1Id, null);
        Assert.assertNull(foundTaskDTONull);
    }

    @Test
    @DisplayName("Поиск задачи по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullTest() throws Exception {
        @Nullable final TaskDTO task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Поиск задачи по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull List<TaskDTO> taskListForUser = taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= taskListForUser.size(); i++) {
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(testUser1Id, i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Поиск задачи по индексу равному Null")
    public void findOneByIndexNullForUserText() throws Exception {
        @Nullable final TaskDTO task = taskRepository.findOneByIndex(testUser1Id, null);
        Assert.assertNull(task);
    }

    @Test
    @DisplayName("Получить количество задач")
    public void getSizeTest() throws Exception {
        int actualSize = taskRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .count();
        int actualSize = taskRepository.getSize(testUser1Id);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить задачу")
    public void removeTest() {
        @Nullable final TaskDTO task = taskList.get(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final TaskDTO deletedTaskDTO = taskRepository.remove(task);
        Assert.assertNotNull(deletedTaskDTO);
        @Nullable final TaskDTO deletedTaskDTOInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskDTOInRepository);
    }

    @Test
    @DisplayName("Удалить Null задачу")
    public void removeNullTest() {
        @Nullable final TaskDTO task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

}