package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.repository.dto.UserDtoRepository;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@DisplayName("Тестирование создания схемы для пользователей")
public class UserSchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("user");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @NotNull final UserDTO user = createUser();
        Assert.assertNotNull(user);
        int countOfUsers = getCountOfUsers();
        Assert.assertEquals(1, countOfUsers);
        deleteUser(user);
    }

    @NotNull
    private UserDTO createUser() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        @NotNull final UserDTO user = new UserDTO();
        userRepository.add(user);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    private int getCountOfUsers() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        final int count = userRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteUser(@NotNull final UserDTO user) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}